import json
import multiprocessing
import os

host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("ZOU_PORT", "5000")
bind_env = os.getenv("BIND", None)
use_loglevel = os.getenv("LOG_LEVEL", "info")
if bind_env:
    use_bind = bind_env
else:
    use_bind = f"{host}:{port}"

# Gunicorn config variables
loglevel = use_loglevel
accesslog = "/var/log/zou/zou_access.log"
errorlog = "/var/log/zou/zou_error.log"
workers = 3
bind = use_bind
keepalive = 120
worker_class = "gevent"
timeout = 600

# For debugging and testing
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    # Additional, non-gunicorn variables
    "host": host,
    "port": port,
}
print(json.dumps(log_data))
