import json
import multiprocessing
import os

host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("ZOU_EVENTS_PORT", "5001")
bind_env = os.getenv("BIND", None)
use_loglevel = os.getenv("LOG_LEVEL", "info")
if bind_env:
    use_bind = bind_env
else:
    use_bind = f"{host}:{port}"

# Gunicorn config variables
loglevel = use_loglevel
accesslog = "/var/log/zou/zou_events_access.log"
errorlog = "/var/log/zou/zou_events_error.log"
workers = 1
bind = use_bind
keepalive = 120
worker_class = "geventwebsocket.gunicorn.workers.GeventWebSocketWorker"

# For debugging and testing
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    # Additional, non-gunicorn variables
    "host": host,
    "port": port,
}
print(json.dumps(log_data))
