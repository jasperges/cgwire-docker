#! /usr/bin/env sh
set -e

# Start Gunicorn
# Zou event stream
gunicorn -c "$GUNICORN_EVENTS_CONF" "$APP_EVENTS_MODULE" &
# Zou API
exec gunicorn -c "$GUNICORN_CONF" "$APP_MODULE"
