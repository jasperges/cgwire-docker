#!/usr/bin/env sh
set -e

export APP_MODULE=zou.app:app
export APP_EVENTS_MODULE=zou.event_stream:app
export GUNICORN_CONF=/app/gunicorn_conf.py
export GUNICORN_EVENTS_CONF=/app/gunicorn_events_conf.py

exec "$@"
